from django.urls import include, path
from .views import *

urlpatterns = [
    path('', about, name='about'),
    path('education/', education, name='education'),
    path('skill/', skill, name='skill')
]