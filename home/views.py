from django.shortcuts import render

# Create your views here.
def about(request):
    return render(request, "Profile.html")

def education(request):
    return render(request, "Education.html")

def skill(request):
    return render(request, "Skill.html")